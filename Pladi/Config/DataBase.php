<?php namespace Pladi\Config;

	class DataBase
	{

		/*		DRIVER DE CONEXION 		*/
		
		public static $driver   = "mysql";
		
		/*	**	*/
		
		/*		HOST 		*/
		
		public static $host     = "localhost";
		
		/*	**	*/
		
		/*		USUARIO BD 		*/
		
		public static $user     = "homestead";
		
		/*	**	*/
		
		/*		CONTRASEÑA BD 		*/
		
		public static $pass     = "secret";
		
		/*	**	*/
		
		/*		NOMBRE DE LA BD 		*/
		
		public static $database = "pladi";
		
		/*	**	*/
		
		/*		CHARSET 		*/
		
		public static $charset  = "utf8";
		
		/*	**	*/

	}
/*		FIN CLASS DB 		*/
